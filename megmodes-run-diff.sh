#!/bin/bash

#              __.-/|
#              \`o_O'
#               =( )=  +-----+
#                 U|   | WIP |
#       /\  /\   / |   +-----+
#      ) /^\) ^\/ _)\     |
#      )   /^\/   _) \    |
#      )   _ /  / _)  \___|_
#  /\  )/\/ ||  | )_)\___,|))
# <  >      |(,,) )__)    |
#  ||      /    \)___)\
#  | \____(      )___) )____
#   \______(_______;;;)__;;;)


sid=$1

## 'final' dir on hdd and work dir on ssd
fdir=/tmp/hcp/$sid/dwi
wdir=/tmp/hcp/$sid/dwi

set -eu
if [[ -f $fdir/10M.tck ]]; then echo $sid done, exit early!; exit 0; fi
pushd $wdir

. ${FSLDIR}/etc/fslconf/fsl.sh

# data already downloaded with megmodes-pull.py $sid
pwd
ls -lh 

function mk() {
        if [[ ! -f "$1" ]]; then
                shift 1
                $@
        fi
}

mk dwi.mif mrconvert -fslgrad bvecs bvals data.nii.gz dwi.mif -force
mk t1.mif mrconvert T1w_acpc_dc_restore.nii.gz t1.mif -force

# cf https://osf.io/fkyht BATMAN protocol w/ mrtrix
# assuming eddy correct & coreg w/ T1 already, since it's HCP
mk mask.mif dwi2mask dwi.mif mask.mif -force
mk csf.txt dwi2response dhollander dwi.mif wm.txt gm.txt csf.txt -nthreads 4 -force
mk csffod.mif dwi2fod msmt_csd dwi.mif -mask mask.mif wm.txt wmfod.mif gm.txt gmfod.mif csf.txt csffod.mif -nthreads 4 -force
mk 5tt.mif 5ttgen fsl t1.mif 5tt.mif -force
mk gwmwi.mif 5tt2gmwmi 5tt.mif gwmwi.mif -force
mk 10M.tck tckgen -act 5tt.mif -backtrack -seed_gmwmi gwmwi.mif -select 10M wmfod.mif 10M.tck -nthreads 4 -force

# no sift since we're going to do vertex wise connectivity
popd
