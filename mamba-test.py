# this is the basic example from 
# https://github.com/state-spaces/mamba
# it does *not* work on CPU

import time
import numpy as np
mm = np.lib.format.open_memmap('megmodes-100307.npy')
dim = 20
mm_ = mm[:1024, :dim].copy() # copy or we mess up the npy file
mm_ /= mm_.ptp()
t = np.r_[:len(mm_)]/678.0

import matplotlib.pyplot as pl
pl.figure(figsize=(20, 15))
pl.plot(t, mm_ + np.r_[:dim], 'k')
pl.grid(1)
pl.savefig('mamba-win0.jpg')

"""
nt, dim = mm.shape
mm /= mm.std(axis=0)
mm = np.clip(mm, -3.0, 3.0) # clip at z=+/-3
length = 1024
nwin = nt // length
mmw = mm[:nwin*length].reshape((nwin, length, dim))
print('data shape', mmw.shape)
"""

import torch
from mamba_ssm import Mamba

# batch, length, dim = 2, 64, 16
# x = torch.randn(batch, length, dim).to("cuda")

batch = 1 # 32
#t_mmw = torch.from_numpy(mmw).to("cuda")


model = Mamba(
    # This module uses roughly 3 * expand * d_model^2 parameters
    d_model=dim, # Model dimension d_model
    d_state=16,  # SSM state expansion factor
    d_conv=4,    # Local convolution width
    expand=4,    # Block expansion factor
).to("cuda")

# loss = lambda i : torch.sum(torch.square(model(t_mmw[i:batch+i]) - t_mmw[i+1:batch+i+1]))
def loss(i):
    t_win = torch.from_numpy(mm_[None]).to('cuda')
    x = t_win[:,:678]
    y = t_win[:,678:]
    y_hat = model(x)
    err = y - y_hat[:,-346:]
    return torch.sum(torch.square(err))
    

opt = torch.optim.SGD(model.parameters(), lr=1e-3, momentum=0.9)
print('initial log loss:', np.log(loss(0).item()))
tik = time.time()
niter = 10000
for i in range(niter):
    opt.zero_grad()
    sse = loss(i)
    sse.backward()
    opt.step()
    total_norm = 0.0
    for p in model.parameters():
        param_norm = p.grad.detach().data.norm(2)
        total_norm += param_norm.item() ** 2
        total_norm = total_norm ** (1. / 2)
    if i % 1000 == 0:
        nbps = batch*(i+1)/(time.time() - tik)
        print(f'{i}/{niter} ll {np.log(sse.item()):0.2f} |g| {total_norm:0.2f} nbps {nbps:0.1f}')

        pl.close('all')
        pl.figure(figsize=(20, 15))
        pl.plot(t, mm_ + np.r_[:dim], 'k')
        t_win = torch.from_numpy(mm_[None]).to('cuda')
        y_hat = model(t_win[:,:678])
        pl.plot(t[-678:], y_hat.detach().to('cpu').numpy()[0] + np.r_[:dim], 'b')
        pl.grid(1)
        pl.savefig('mamba-win0.jpg')
