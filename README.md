# megmodes

MEG data prep for modeling with neural field-ish. 

- needs a HCP AWS S3 key, go get one!
- uses Brainstorm to run preprocessing but can work with MCR
- rest is done with Python

# containerz

to make this more portable, we're using apptainer, but e.g. Docker would be possible.

One time build step:
```bash
apptainer build -F megmodes.{sif,def}
apptainer build -F brainstorm.{sif,def}
```

# layout

- `brainstorm3-template-folders` contains stuff Brainstorm needs to run first time
- `brainstorm3-versions` contains a known working Brainstorm version
- `hcp-meg-ids.txt` lists HCP subject IDs which have MEG
- `megmodes-badchans.json` compiled list of bad channels in JSON format, all subjects all runs
- `megmodes-compile-badchans.py` generates `megmodes-badchans.json`
- `megmodes_dataloader.py` WIP dataloader for e.g. Torch
- `megmodes.def` apptainer def for running Brainstorm in container
- `megmodes-run-diff.sh` WIP make connectome from dwi w/ mrtrix3

files which need to be run by hand ftm:

- `megmodes-pull.py` downloads data from HCP AWS S3
- `megmodes-prep.sh` invokes `megmodes_prep.m` w/ MCR+Brainstorm to preprocess MEG data & compute gain matrix
- `megmodes-export.py` combines all runs' time series into a single numpy file

# run preprocessing

so assuming you have an appropriate `~/.aws/credentials` file, getting a single subjects'
chunk of data takes about 20 minutes with the following commands,

```bash
$ ./megmodes.sif python megmodes-pull.py 100307
👤 100307 ....................................................................

$ ./brainstorm.sif bash megmodes-prep.sh 100307
... lots of output ...

$ ls /tmp/brainstorm_db/hcp/data/100307/*/run_data.mat | nl
...
     6	/tmp/brainstorm_db/hcp/data/100307/@raw8-StoryM_c_rfDC_notch_band_clean/run_data.mat

$ ./megmodes.sif python megmodes-export.py 100307
100307 has 6 runs, gdist, lc, 0, 1, 2, 3, 4, 5, megmodes-100307.npy done.

$ ls -lh meg*.npy
-rw-rw-r-- 1 duke duke 729M déc.  13 16:31 megmodes-100307.npy
```

# running Brainstorm script in container

Assuming some data already got from HCP AWS S3 in `/work/duke/hcp-meg`, we can
preprocess it with the Brainstorm script `megmodes_prep.m` with our container like so
```
apptainer exec --bind=/work/duke/hcp-meg:/tmp/hcp:ro megmodes.sif bash megmodes-prep.sh 872764
```
