#!/bin/bash

sid=$1

set -eux

# TODO bring some of these files into the container

brainstorm=./brainstorm3-versions/231208/brainstorm3/bin/R2022b/brainstorm3.command

# on my machine, default install
# MCR=/usr/local/MATLAB/MATLAB_Runtime/R2022b

# make sure we have the bst config etc
if [[ ! -d ~/.brainstorm ]] || [[ ! -d /tmp/brainstorm_db ]]
then
	echo "creating necessary brainstorm config & db exist"
	pushd brainstorm3-template-folders
	bash restore.sh
	popd
fi

# run the script
${brainstorm} ${MCR} megmodes_prep.m ${sid}

# clean up unused
rm -rf /tmp/brainstorm_db/hcp/data/*/@raw*_notch
rm -rf /tmp/brainstorm_db/hcp/data/*/@raw*_notch_band
