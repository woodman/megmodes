import boto3, os, json, sys

sid, = sys.argv[1:]

client = boto3.client('s3')
workpath = '/tmp/hcp'
anat_dir = f'{workpath}/{sid}/anatomy'
raw_dir = f'{workpath}/{sid}/raw'
dwi_dir = f'{workpath}/{sid}/dwi'
verbose = False

os.makedirs(anat_dir, exist_ok=True)
os.makedirs(raw_dir, exist_ok=True)
os.makedirs(dwi_dir, exist_ok=True)

def make_download_list(sid):
    downloads = []

    anat_objects = client.list_objects(
        Bucket='hcp-openaccess',
        Prefix=f'HCP_1200/{sid}/MEG/anatomy')

    meg_objects = client.list_objects(
        Bucket='hcp-openaccess',
        Prefix=f'HCP_1200/{sid}/unprocessed/MEG')

    # dwi_objects = client.list_objects(
    #    Bucket='hcp-openaccess',
    #    Prefix=f'HCP_1200/{sid}/T1w/Diffusion')
    # HCP_1200/100307/T1w/Diffusion/bvals
    # HCP_1200/100307/T1w/Diffusion/bvecs
    # HCP_1200/100307/T1w/Diffusion/data.nii.gz
    # HCP_1200/$sid/T1w/T1w_acpc_dc_restore.nii.gz t1.mif

    for dwi_fname in 'bvals bvecs data.nii.gz'.split(' '):
        downloads.append((
            f'HCP_1200/{sid}/T1w/Diffusion/{dwi_fname}',
            os.path.join(dwi_dir, dwi_fname)
            ))
    downloads.append((
        f'HCP_1200/{sid}/T1w/T1w_acpc_dc_restore.nii.gz',
        os.path.join(dwi_dir, 'T1w_acpc_dc_restore.nii.gz')
        ))


    # TODO download bad channels files

    for s3obj in anat_objects['Contents']:
        s3path = s3obj['Key']
        dlpath = os.path.join(anat_dir, os.path.basename(s3path))
        downloads.append((s3path, dlpath))

    for obj in meg_objects['Contents']:
        key = obj['Key']
        fname = os.path.basename(key)
        _hcp, _sid, _unproc, _meg, task, _4d = os.path.dirname(key).split(os.sep)
        dlpath = f'{raw_dir}/{task}/{fname}'
        os.makedirs(os.path.dirname(dlpath), exist_ok=True)
        downloads.append((key, dlpath))

    return downloads

downloads = make_download_list(sid)
print(f'👤 {sid} ', end='', flush=True)
for s3path, dlpath in downloads:
    if not os.path.exists(dlpath):
        client.download_file('hcp-openaccess', s3path, dlpath)
    print('.', end='', flush=True)
print('.')

